#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <wait.h>

int main()
{
	pid_t pid1, pid2, pid3, pid4, pid5, pid6, pid7;
	int status;
	// create drakor directory
	pid1 = fork();
	if (pid1 < 0)
		exit(EXIT_FAILURE);
	if (pid1 == 0)
	{
		char *argv[] = {"mkdir", "-p", "/home/aryaw/shift2/drakor", NULL};
		char path[] = "/bin/mkdir";
		execv(path, argv);
	}

	// unzip drakor.zip
	while ((wait(&status)) > 0);
	pid2 = fork();
	if (pid2 < 0)
		exit(EXIT_FAILURE);
	if (pid2 == 0)
	{
		char *argv[] = {"unzip", "-j", "/home/aryaw/drakor.zip", "*.png", "-d", "/home/aryaw/shift2/drakor", NULL};
		char path[] = "/usr/bin/unzip";
		execv(path, argv);
	}

	return 0;
}


// # get genre and create directory and txt file
// ls *.png 
// | awk -F'[_]' '{ for (i=1; i<=NF; i++) print $i }'
// | awk -F'[;.]' '{print $3}' 
// | sort 
// | uniq 
// | while read line; do `mkdir $line`; `touch /home/$USER/shift2/drakor/$line/data.txt`; `printf "kategori : %s\n\n" $line >> /home/$USER/shift2/drakor/$line/data.txt`; done

// # append nama dan rilis ke data.txt, copy dan rename poster
// ls *.png 
// | awk -F'[_]' '{ for (i=1; i<=NF; i++) print $i }'
// | awk -F'[;.]' '{printf "%s;%s;%s\n", $1, $2, $3}'
// | sort -k2 -n | while read line; do name=`printf $line
// | awk -F'[;]' '{print  $1}'`; year=`printf $line
// | awk -F'[;]' '{print  $2}'`; genre=`printf $line
// | awk -F'[;]' '{print  $3}'`; filename=`ls *.png
// | awk -v pat="$name" '$0~pat'`; printf "nama : %s\nrilis : tahun %s\n\n" $name $year >> /home/$USER/shift2/drakor/$genre/data.txt; cp $filename /home/$USER/shift2/drakor/$genre/$name.png; done

// # delete file original
// rm *.png
