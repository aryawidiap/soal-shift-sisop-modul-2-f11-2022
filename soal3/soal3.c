#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <wait.h>
#include <pwd.h>
#include <dirent.h>
void daemon_process(){
      pid_t pid, sid;

  pid = fork();

  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
}

void toList(){
    DIR *fd;
    struct dirent *dir;
    char fileformat[300], fs[300];
    FILE *fileptr;
    struct stat info;
    int r;
    
    fileptr = fopen("/home/olanemon/modul2/air/list.txt", "a");
    fd = opendir("/home/olanemon/modul2/air");

    if (fd != NULL){
        while ((dir = readdir (fd))) {
          if (strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0){
              sprintf(fs, "/home/olanemon/modul2/air/%s", dir->d_name);
              r = stat(fs, &info);
              if( r == -1 ){
                fprintf(stderr,"File error\n");
                exit(1);
              }
              struct passwd *pw = getpwuid(info.st_uid);
              if(pw != 0){
              strcpy(fileformat, pw->pw_name);
              strcat(fileformat, "_");
              if( info.st_mode & S_IRUSR ) strcat(fileformat, "r");
              if( info.st_mode & S_IWUSR ) strcat(fileformat, "w");
              if( info.st_mode & S_IXUSR ) strcat(fileformat, "x");
              strcat(fileformat, "_");
              strcat(fileformat, dir->d_name);
              fprintf(fileptr, "%s\n", fileformat);
            }
          }
          closedir(fd)
        }
    }
    fclose(fileptr);
}

void process(){
  pid_t pids[10];
  int i = 0;
  int status;
  for(i = 0; i < 9; i++){
    pids[i] = fork();
    if(pids < 0){   //fork gagal
      exit(EXIT_FAILURE);
    }
    else if(i == 0){ // buat folder darat dan air
      if(pids[i] == 0){ // buat folder darat
        char *args[] = {"mkdir","-p","/home/olanemon/modul2/darat", NULL};
        execv("/usr/bin/mkdir", args);
      }else{ // buat folder air
        while ((wait(&status)) > 0);
        sleep(3);
        char *args[] = {"mkdir","-p","/home/olanemon/modul2/air", NULL};
        execv("/usr/bin/mkdir", args);
      }
    }
    else if(i == 1 && pids[i] == 0){ // download animal
      char *url_new = malloc(128 * sizeof(char));
      sprintf(url_new , "https://drive.google.com/uc?export=download&id=1jy1v50CwL0nVRCnniSkYslLHLEm1g9wR");
      execl("/usr/bin/wget", "/user/bin/wget", "--no-check-certificate", url_new, "-O", "/home/olanemon/modul2", NULL);
      free(url_new);
      exit(0);
      // char *args[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1jy1v50CwL0nVRCnniSkYslLHLEm1g9wR", ">", "animal.zip", NULL};
      // execv("usr/bin/wget", args);
    }
    else if(i == 2){ // extract animal
      if(pids[i] == 0){
        char *args[] = {"unzip" ,"/home/olanemon/animal.zip", "-d","/home/olanemon/modul2", NULL};
        execv("/usr/bin/unzip", args);
      }
    }
    else if(i == 3 && pids[i] == 0){ // pindah animal darat ke folder
      //find /home/$USER/modul2/animal -name '*darat*' -exec mv -t /home/$USER/modul2/darat {} +
      char *args[] = {"find","/home/olanemon/modul2/animal", "-name", "'*darat*'", "-exec", "mv", "-t", "/home/modul2/darat", "{}", "+", NULL};
      execv("/usr/bin/find", args);
    }
    else if(i == 4 && pids[i] == 0){ // pindah animal air ke folder
      char *args[] = {"find","/home/olanemon/modul2/animal", "-name", "'*air*'", "-exec", "mv", "-t", "/home/modul2/air", "{}", "+", NULL};
      execv("/usr/bin/find", args);
    }
    else if(i == 5 && pids[i] == 0){ // delete animal dan isi yang tidak masuk di darat dan air
      char *args[] = {"rm", "-rf", "/home/olanemon/modul2/animal", NULL};
      execv("/bin/rm", args);
    }
    else if(i == 6 && pids[i] == 0){ // remove bird di folder darat
      //find /home/$USER/modul2/darat -name '*bird*' -exec rm {} +
      char *args[] = {"find", "/home/olanemon/modul2/darat", "-name", "'*bird*'", "-exec", "rm", "{}", "+", NULL};
      execv("/bin/find", args);
    }
    else if(i == 7 && pids[i] == 0){ // buat list.txt
      char *args[] = {"touch","-p","/home/olanemon/modul2/air/list.txt", NULL};
      execv("/bin/touch", args);
    }
    else if(i == 8 && pids[i] == 0){ // append text ke list.txt
       toList();
    }
  }
  while (wait(&status) > 0) {
    }
}


int main(){
    daemon_process();

    while(1){
      pid_t child_id;
      int status;
      child_id = fork();
      if(child_id < 0){
        exit(EXIT_FAILURE);
      }
      if(child_id == 0){
        process();
      }
      else{
         while ((wait(&status)) > 0);
      }
    }
    return 0;
}
