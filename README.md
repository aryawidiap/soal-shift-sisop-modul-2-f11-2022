# soal-shift-sisop-modul-2-f11-2022

Anggota Kelompok

 - Arya Widia Putra     	5025201016
 - Muhammad Rolanov Wowor	5025201017
 - Muhammad Damas Abhirama	5025201271

## Soal 1

1a. mendownload databas characters (https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view) dan weapons (https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view)

![Gambar-fungsi-main](/img/1_main.png)
<br>
Screenshot fungsi main

Memanggil fungsi download_file. didalam fungsi ini menggunakan execl wget untuk mendownload file dari link

![Gambar-fungsi-download_file](/img/1_fungsiDownload.png)
<br>
Screenshot fungsi download_file

Memanggil fungsi unzip_file. didalam fungsi ini menggunakan execl unzip untuk men-unzip file yang telah didownload yaitu "Anggap_ini_database_characters.zip" dan "Anggap_ini_database_weapons.zip"

![Gambar-fungsi-unzip_file](/img/1_fungsiUnzip.png)
<br>
Screenshot fungsi unzip_file

Memanggil fungsi create_folder untuk membuat file directory. Didalam fungsi ini menggunakan execl mkdir untuk membuat directory baru dengan nama "gacha_gacha"

![Gambar-fungsi-create_folder](/img/1_fungsiCreateFolder.png)
<br>
Screenshot fungsi create_folder

Setelah itu mengganti working directory menjadi di dalam folder "gacha_gacha" menggunakan chdir("gacha_gacha")

![Gambar-Hasil-Program](/img/1_programResult.png)
<br>
Screenshot hasil jalannya program

Kendala yang dialami saat pengerjaan soal shift:

1. Tidak bisa melakukan download di soal 1a

Kendala yang dialami saat revisi soal shift:

1. Belum bisa melakukan parsing json


## Soal 2

2a. Mengekstrak file dengan extension .png dari drakor.zip ke `/home/$USER/shift2/drakor`

Kondisi home directory sebelum program dijalankan

![Sebelum program dijalankan](img/2_sebelum-program.jpg)


```c
// create drakor directory
pid1 = fork();
if (pid1 < 0)
	exit(EXIT_FAILURE);
if (pid1 == 0)
{
	char *argv[] = {"mkdir", "-p", "/home/aryaw/shift2/drakor", NULL};
	char path[] = "/bin/mkdir";
	execv(path, argv);
}
```

Membuat directory tujuan terlebih dahulu sebelum mengekstrak file. -p berarti program akan membuat semua folder yang perlu sampai dapat dibuat folder drakor.

Pembuatan folder /home/aryaw/shift2/drakor

![Hasil mkdir](img/2_mkdir.png)

```c
// unzip drakor.zip
while ((wait(&status)) > 0);
pid2 = fork();
if (pid2 < 0)
	exit(EXIT_FAILURE);
if (pid2 == 0)
{
	char *argv[] = {"unzip", "-j", "/home/aryaw/drakor.zip", "*.png", "-d", "/home/aryaw/shift2/drakor", NULL};
	char path[] = "/usr/bin/unzip";
	execv(path, argv);
}
```

Mengekstrak file dari drakor.zip hanya yang berekstensi .png, dapat dilihat pada argv sebagai elemen ke 4.

Proses unzip

![Proses unzip](img/2_unzip.png)

Hasil unzip ke folder drakor

![Isi folder drakor](img/2_isi-folder-drakor.jpg)

Kendala yang dialami saat pengerjaan soal shift :

1. Belum paham mengenai pipe dengan menggunakan bahasa C, padahal saat menggunakan CLI, banyak menggunakan pipe.

2. Kurang paham dengan variabel atau character yang dapat digunakan pada CLI, tetapi tidak dapat digunakan pada fungsi execv.

3. Tidak berhasil mengunduh file dengan command terminal

## Soal 3
3a. Membuat folder darat, kemudian 3 detik setelah itu membuat folder air
```
 pids[i] = fork();
    if(pids < 0){   //fork gagal
      exit(EXIT_FAILURE);
    }
    else if(i == 0){ // buat folder darat dan air
      if(pids[i] == 0){ // buat folder darat
        char *args[] = {"mkdir","-p","/home/olanemon/modul2/darat", NULL};
        execv("/usr/bin/mkdir", args);
      }else{ // buat folder air
        while ((wait(&status)) > 0);
        sleep(3);
        char *args[] = {"mkdir","-p","/home/olanemon/modul2/air", NULL};
        execv("/usr/bin/mkdir", args);
      }
```
![Gambar-create-folder](/img/3_createfolder.png)
<br>
Pembuatan folder darat dan air yang berjarak 3 detik

3b. extract folder animal
```
    else if(i == 1){ // extract animal
      if(pids[i] == 0){
        char *args[] = {"unzip" ,"/home/olanemon/animal.zip", "-d","/home/olanemon/modul2", NULL};
        execv("/usr/bin/unzip", args);
      }
    }
```

3c. Memindahkan isi folder animal sesuai kriterianya, ke folder darat dan ke folder air
```
else if(i == 2 && pids[i] == 0){ // pindah animal darat ke folder
      //find /home/$USER/modul2/animal -name '*darat*' -exec mv -t /home/$USER/modul2/darat {} +
      char *args[] = {"find","/home/olanemon/modul2/animal", "-name", "'*darat*'", "-exec", "mv", "-t", "/home/modul2/darat", "{}", "+", NULL};
      execv("/usr/bin/find", args);
else if(i == 3 && pids[i] == 0){ // pindah animal air ke folder
      char *args[] = {"find","/home/olanemon/modul2/animal", "-name", "'*air*'", "-exec", "mv", "-t", "/home/modul2/air", "{}", "+", NULL};
      execv("/usr/bin/find", args);
    }
```
file yang tidak memenuhi kriteria darat ataupun air akan dihapus
```
else if(i == 4 && pids[i] == 0){ // delete animal dan isi yang tidak masuk di darat dan air
      char *args[] = {"rm", "-rf", "/home/olanemon/modul2/animal", NULL};
      execv("/bin/rm", args);
    }
```
3d. Menghapus file bird yang berada di folder darat
```
else if(i == 5 && pids[i] == 0){ // remove bird di folder darat
      //find /home/$USER/modul2/darat -name '*bird*' -exec rm {} +
      char *args[] = {"find", "/home/olanemon/modul2/darat", "-name", "'*bird*'", "-exec", "rm", "{}", "+", NULL};
      execv("/bin/find", args);
    }
```
3e. Membuat file list, dan menambah isi folder air ke textfile list
```
    else if(i == 6 && pids[i] == 0){ // buat list.txt
      char *args[] = {"touch","/home/olanemon/modul2/air/list.txt", NULL};
      execv("/bin/touch", args);
    }
    else if(i == 7 && pids[i] == 0){ // append text ke list.txt
       toList();
    }
```
fungsi toList :
```
void toList(){
    DIR *fd;
    struct dirent *dir;
    char fileformat[300], fs[300];
    FILE *fileptr;
    struct stat info;
    int r;
    
    fileptr = fopen("/home/olanemon/modul2/air/list.txt", "a");
    fd = opendir("/home/olanemon/modul2/air");

    if (fd != NULL){
        while ((dir = readdir (fd))) {
          if (strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0){
              sprintf(fs, "/home/olanemon/modul2/air/%s", dir->d_name);
              r = stat(fs, &info);
              if( r == -1 ){
                fprintf(stderr,"File error\n");
                exit(1);
              }
              struct passwd *pw = getpwuid(info.st_uid);
              if(pw != 0){
              strcpy(fileformat, pw->pw_name);
              strcat(fileformat, "_");
              if( info.st_mode & S_IRUSR ) strcat(fileformat, "r");
              if( info.st_mode & S_IWUSR ) strcat(fileformat, "w");
              if( info.st_mode & S_IXUSR ) strcat(fileformat, "x");
              strcat(fileformat, "_");
              strcat(fileformat, dir->d_name);
              fprintf(fileptr, "%s\n", fileformat);
            }
          }
        }
    }
    fclose(fileptr);
}
```
![Gambar-hasil-list](img/3_isilist.png)
<br>
isi folder air tersimpan dalam textfile list.txt

Kendala yang dialami saat pengerjaan soal shift :

1. Belum bisa menyelesaikan soal 3e, yaitu membuat textfile berisi file dengan format UID_[UID file permission]_Nama File.[jpg/png]

2. Implementasi dalam bahasa C, source code dengan bahasa C bisa berjalan namun tidak mengeluarkan hasil yang diinginkan

Kendala yang dialami saat revisi :
1. Fungsi download wget yang tidak bisa berfungsi dengan baik, baik dari terminal command maupun source code. Zipfile yang terdownload tidak bisa diextract, dan berukuran 0 KB
2. Fungsi toList melakukan infinite looping, sehingga append ke textfile sangat banyak dan terus bertambah bila tidak di break
