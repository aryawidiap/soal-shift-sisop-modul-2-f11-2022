#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>
#include <wait.h>
#include <time.h>

// Fungsi untuk download file dari drive
void download_file(char *url, char *path)
{
    pid_t child_id = fork();    // pid_t = Tipe data ID proses (signed integer)
    if (child_id == 0)
    {
        char *url_new = malloc(128 * sizeof(char));   // Memberikan blok memori dalam byte -stdlib
        sprintf(url_new, "https://drive.google.com/uc?id=%s&export=download", url); // printf tapi simpan di buffer
        printf("Downloading file : %s\n", url);
        execl("/usr/bin/wget", "/user/bin/wget", "--no-check-certificate", url_new, "-O", path, NULL);
        free(url_new);
        exit(0);
    } else 
    {
        int status = 0;
        waitpid(child_id, &status, 0);
    }
}

// Fungsi untuk unzip file yang telah didownload
void unzip_file(char *path)
{
    pid_t child_id = fork();
    if(child_id == 0)
    {
        printf("Unzip file : %s\n", path);
        execl("/usr/bin/unzip", "/usr/bin/unzip", path, NULL);
        exit(0);
    } else 
    {
        int status = 0;
        waitpid (child_id, &status, 0);
    }
}

// Fungsi membuat folder/directory
void create_folder(char *path)
{
    pid_t child_id = fork();
    if(child_id == 0)
    {
        printf("Create folder : %s\n", path);
        remove(path);
        execl("/bin/mkdir", "/bin/mkdir", path, NULL);
        exit(0);
    } else 
    {
        int status = 0;
        waitpid (child_id, &status, 0);
    }
}

int main()
{
    // Download dari drive
    // characters: https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view
    // weapons: https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view
    download_file("1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "Anggap_ini_database_characters.zip");
    download_file("1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "Anggap_ini_database_weapons.zip");

    // Unzip File Zip
    unzip_file("Anggap_ini_database_characters.zip");
    unzip_file("Anggap_ini_database_weapons.zip");

    // Create Folder "gacha_gacha"
    create_folder("gacha_gacha");

    chdir("gacha_gacha");
}
